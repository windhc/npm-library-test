import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  plugins: [vue()],

  // 配置打包入口出口
  build: {
    // 入口
    lib: {
      //指定组件编译入口文件
      entry: "./src/index.ts",
      // 需要提供一个name 这个名字尽量不和 npm 上发布的包名一致，否则也会推送不到npm
      name: "VueTestHello",
      fileName: 'vue-test-hello',
    },
    rollupOptions: {
      //打包的时候不需要打包的依赖
      external: ["vue"],
      // 打包抛出一个全局方法
      output: {
        globals: {
          vue: "Vue"
        }
      }
    }
  }
})
