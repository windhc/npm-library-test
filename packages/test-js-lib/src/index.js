/**
 * 测试常量
 * @type {string} bbb
 */
export const bbb = "bbb222"

/**
 * 测试函数
 * @param {string} param 入参
 * @returns {string} 结果
 */
export function fun2(param) {
  return param + "---ccc"
}
