# npm-library-test

npm私库开发示例项目

## package.json
 - files   要发布的目录
 - main    项目的入口文件
 - module  ESM的入口文件
 - browser 定义 npm 包在 browser 环境下的入口文件
 - types   ts类型定义文件
 - peerDependencies 该库需要依赖的其他库
 - name 字段为组件名
   - @aa/*  为 aa 项目组 
   - @xx/*  为 xx 项目组


