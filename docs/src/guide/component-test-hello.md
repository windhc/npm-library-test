# TestHello组件

## 简介

测试演示组件

## 安装

```shell
npm install @mapfe/vue-test-hello --save
# or
pnpm install @mapfe/vue-test-hello --save
```

## API

| 名称  | 类型     | 	默认值 | 说明 |
|-----|--------|------|----|
| msg | string |      | 消息 |

## 使用示例

