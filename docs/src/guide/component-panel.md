# MPanel组件

## 简介
带标题的面板组件

## 安装

```shell
npm install @mapfe/vue-test-hello --save
# or
pnpm install @mapfe/vue-test-hello --save
```

## API

| 名称    | 类型     | 	默认值 | 说明   |
|-------|--------|------|------|
| title | string |      | 面板标题 |


## 使用示例

