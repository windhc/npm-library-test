import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "MAP前端组件库",
  description: "",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: '首页', link: '/' },
      { text: '文档', link: '/guide/' }
    ],

    sidebar: [
      {
        text: '组件库',
        items: [
          { text: 'TestHello组件', link: '/guide/component-test-hello' },
          { text: 'MPanel组件', link: '/guide/component-panel' }
        ]
      },
      {
        text: '代码库',
        items: [
          { text: '通用工具函数', link: '/guide/library-utils' },
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://gitlab.mapfarm.com/map-front/map-fe-component' }
    ]
  }
})
