---
layout: home
title: Home
hero:
  name: MAP前端组件库
  tagline: 业务组件，产品组件，效率提升
#  image:
#    src: /logo.svg
#    alt: pinia-plugin-persistedstate
  actions:
    - theme: brand
      text: 文档
      link: /guide/
    - theme: alt
      text: View on GitLab
      link: https://gitlab.mapfarm.com/map-front/map-fe-component
features:
  - title: API友好
    details: 方便易用，文档齐全
  - title: 业务沉淀
    details: 迭代优化，支撑业务发展，方便新人上手
---
